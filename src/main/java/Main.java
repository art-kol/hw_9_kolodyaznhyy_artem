import proxy.CacheProxy;
import model.ListServiceImpl;
import model.Service;
import model.TimeService;

public class Main {

    public static void main(String[] args) throws NoSuchMethodException {

        System.out.println("****** START TASK 1 ******\n");

        CacheProxy cacheProxy = new CacheProxy();

        Service listService = cacheProxy.cache(new ListServiceImpl());
        doTest1(listService);

        System.out.println("****** START TASK 2 ******\n");
        Service timeService = cacheProxy.cache(new TimeService());
        doTest2(timeService);

        System.out.println("****** END ******");

    }

    private static void doTest1(Service listService){
        listService.doWork(10, 100);
        listService.doWork(5, 20);
        listService.doWork(15, 10);
        listService.doWork(10, 100);
    }

    private static void doTest2(Service timeService){
        timeService.doWork("Work1");
        timeService.doWork("Work2");
        timeService.doWork("Work3");
        timeService.doWork("Work1");

    }



}

