package model;

import annotation.Cachable;
import java.util.Date;

public class TimeService implements Service {

    @Cachable(filePrefix = "T", zip = false)
    public Object doWork(Object ... args){
        return new LocalTime(new Date(), null, null);
    }

}
