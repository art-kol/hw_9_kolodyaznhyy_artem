package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

public class LocalTime implements Serializable {

    Object TimeObject;
    Object TimeSerialization;
    Object TimeDeserialization;

    public LocalTime(Object timeObject, Object timeSerialization, Object timeDeserialization) {
        TimeObject = timeObject;
        TimeSerialization = timeSerialization;
        TimeDeserialization = timeDeserialization;
    }

    private void writeObject(ObjectOutputStream out) throws IOException, InterruptedException {
        Thread.sleep(1000);
        TimeSerialization = new Date();
        out.defaultWriteObject();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException, InterruptedException {
        Thread.sleep(1000);
        in.defaultReadObject();
        TimeDeserialization = new Date();
    }

    @Override
    public String toString() {
        return "LocalTime{" +
                "TimeObject=" + TimeObject +
                ", TimeSerialization=" + TimeSerialization +
                ", TimeDeserialization=" + TimeDeserialization +
                '}';
    }
}
