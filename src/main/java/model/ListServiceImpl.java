package model;

import annotation.Cachable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ListServiceImpl implements Service{

    @Cachable(filePrefix = "L", zip = true)
    public Object doWork(Object ... args) {
        List<Integer> randomArray = new ArrayList();
        Random r = new Random();
        int randomInt;

        for (int i = 0; i < (Integer) args[0]; i++) {
            randomInt = r.nextInt((Integer) args[1]);
            randomArray.add(randomInt);
        }
        return randomArray;
    }
}
