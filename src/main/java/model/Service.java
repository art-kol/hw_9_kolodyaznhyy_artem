package model;

public interface Service {
    Object doWork(Object ... args);
}
