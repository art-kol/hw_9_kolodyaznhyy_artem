package proxy;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public final class SerializationService {

    private SerializationService() {
    }

    public static Object readObjectFromCache(String pathReadCache, boolean gzip){
        System.out.println("Возвращаем КЭШированое значение");
        if(gzip){
            decompressFileFromGZip(pathReadCache);
        }

        Object unCachedResult = null;
        try(FileInputStream fis = new FileInputStream(pathReadCache);
            ObjectInputStream ios = new ObjectInputStream(fis)) {

            unCachedResult = ios.readObject();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        if(gzip){
            File binFile = new File(pathReadCache);
            binFile.delete();
        }

        return unCachedResult;
    }

    private static void decompressFileFromGZip(String pathDecompressFile){
        try (GZIPInputStream gis = new GZIPInputStream(
                new FileInputStream(pathDecompressFile+".gz"));
            FileOutputStream fos = new FileOutputStream(pathDecompressFile)) {
                byte[] buffer = new byte[1024];
                int len;
                while ((len = gis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeObjectToCache(Object serviceResult, String pathWriteCache, boolean gzip){
        System.out.println("КЭШируем значение и возвращаем его");
        try (FileOutputStream fos = new FileOutputStream(pathWriteCache);
         ObjectOutputStream oos = new ObjectOutputStream(fos)) {
         oos.writeObject(serviceResult);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(gzip){
            compressFileToGZip(pathWriteCache);
        }
    }

    private static void compressFileToGZip(String pathCompressFile){
        try (GZIPOutputStream gos = new GZIPOutputStream(
                new FileOutputStream(pathCompressFile+".gz"));
            FileInputStream fis = new FileInputStream(pathCompressFile)) {
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    gos.write(buffer, 0, len);
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
        File binFile = new File(pathCompressFile);
        binFile.delete();
    }

}
