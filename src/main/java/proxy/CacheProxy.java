package proxy;

import annotation.Cachable;
import model.Service;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class CacheProxy {

    public Service cache(Service service) throws NoSuchMethodException {
        Method doWork = service.getClass().getMethod("doWork", Object[].class);

        if(!doWork.isAnnotationPresent(Cachable.class)){
            return service;
        }

        Cachable annotation = doWork.getAnnotation(Cachable.class);
        String filePrefix = annotation.filePrefix();
        boolean gzip = annotation.zip();

        return (Service) Proxy.newProxyInstance(service.getClass().getClassLoader(),
                service.getClass().getInterfaces(), new CacheProxyHandler(service,filePrefix,gzip));
    }
}
