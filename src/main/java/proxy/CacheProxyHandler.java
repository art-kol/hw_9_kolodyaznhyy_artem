package proxy;
import model.Service;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class CacheProxyHandler implements InvocationHandler {

    private Service service;
    private String filePrefix;
    private boolean gzip;

    public CacheProxyHandler(Service service, String filePrefix, boolean gzip) {
        this.service = service;
        this.filePrefix = filePrefix;
        this.gzip = gzip;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.print("Обнаружена аннотация Cachable. ");
        PathCache pathCache = new PathCache(args, filePrefix, "src/main/java/cache/");

        if(pathCache.checkCache(pathCache.getPathToCache())){
            System.out.println(SerializationService.readObjectFromCache(pathCache.getPathToCache(), gzip)+"\n");
            return null;
        }

        Object serviceMethod = method.invoke(service, args);

        SerializationService.writeObjectToCache(serviceMethod, pathCache.getPathToCache(), gzip);
        System.out.println(serviceMethod+"\n");

        return serviceMethod;
    }
}
