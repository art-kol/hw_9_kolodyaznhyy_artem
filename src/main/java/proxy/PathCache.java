package proxy;
import java.io.File;
public class PathCache {

    private Object[] newArgs;
    private String filePrefix;
    private String pathFolderCache ;


    public PathCache(Object[] newArgs, String filePrefix, String pathFolderCache) {
        this.newArgs = (Object[]) newArgs[0];
        this.filePrefix = filePrefix;
        this.pathFolderCache = pathFolderCache;
    }

    public String getPathToCache() {
        StringBuilder pathToCache = new StringBuilder(pathFolderCache);
        pathToCache.append(filePrefix);
        for (Object arg: newArgs ){
            pathToCache.append("_").append(arg);
        }
        pathToCache.append(".bin");

        return pathToCache.toString();
    }

    public boolean checkCache(String pathCache){
        System.out.println("Ищем сохраненный результат");
        File dir = new File(pathCache);
        File dirGzip = new File(pathCache+".gz");
        return dir.exists() || dirGzip.exists();
    }
}
